# Introduccion a SQL


## Recursos

### codewars

where clause: https://www.codewars.com/kata/5a8eb3fb57c562110f0000a1

functions (left, right): https://www.codewars.com/kata/5943a58f95d5f72cb900006a

functions (repeat, reverse): https://www.codewars.com/kata/59414360f5c3947364000070

min/max: https://www.codewars.com/kata/581113dce10b531b1d0000bd

## edx

https://www.edx.org/course/querying-data-with-transact-sql-4

## udemy

https://www.udemy.com/courses/search/?src=ukw&q=sql&p=1&price=price-free&courseLabel=7866

https://www.udemy.com/course/introduction-to-databases-and-sql-querying/

https://www.udemy.com/course/microsoft-sql-server-an-introduction-2018-edition/

https://www.udemy.com/course/sql-for-newcomers-free/

## codecademy

https://www.codecademy.com/search?query=sql

https://www.codecademy.com/learn/learn-sql

https://www.codecademy.com/learn/sql-table-transformation

